function pobarvaj(ev, latlngs, polygons) {
  for (var i = 0; i < Object.keys(latlngs).length; i++) {
    for (var j = 0; j < latlngs[i].length; j++) {
      for (var k = 0; k < latlngs[i][j].length; k++) {
        var dist = distance(latlngs[i][j][k][0], latlngs[i][j][k][1], ev.latlng.lat, ev.latlng.lng, "K");
        if (dist < 0.5) {
          polygons[i][j].setStyle({color: 'green'});
          break;
        }
        else {
          polygons[i][j].setStyle({color: 'blue'});
        }
      }
    }
  }
}

$(document).ready(function() {
  
  var map = new L.map('map', {
    center: [46.05004, 14.46931],
    zoom: 12
  });
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  map.addLayer(layer);
  
  $.getJSON("https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", function(data) {
    var bolnisnice = data.features;
    var polygons = {};
    var latlngs = {};
    for (var i = 0; i < bolnisnice.length; i++) {
      if (bolnisnice[i].geometry.type === "Polygon") {
        polygons[i] = [];
        latlngs[i] = [];
        var koordinate = bolnisnice[i].geometry.coordinates;
        var info = bolnisnice[i].properties;
        for (var j = 0; j < koordinate.length; j++) {
          var lnglat = koordinate[j];
          var latlng = [];
          for (var k = 0; k < lnglat.length; k++) {
            latlng.push([lnglat[k][1], lnglat[k][0]]);
          }
          var polygon = L.polygon(latlng, {color: 'blue'}).addTo(map);
          polygon.bindPopup('<p>' + (info["name"] ? info["name"] : "Ni podatka o imenu") + '<br />' +
            (info["addr:street"] ? info["addr:street"] + (info["addr:housenumber"] ? " " + info["addr:housenumber"] : "") +
            (info["addr:city"] ? ", " + info["addr:city"] : "") : "Ni podatka o naslovu") + ' '  + '</p>');
          polygon.on('click', function(ev) {
            pobarvaj(ev, latlngs, polygons);
          });
          latlngs[i].push(latlng);
          polygons[i].push(polygon);
        }
      }
    }
    
    map.on('click', function(ev) {
      pobarvaj(ev, latlngs, polygons);
    });
  });
});