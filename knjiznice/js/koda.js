
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {

  if (stPacienta == 1) {
    var ime = "Janko";
    var priimek = "Medved";
    var datumRojstva = "1951-02-25"
  }
  
  else if (stPacienta == 2) {
    var ime = "Patricija";
    var priimek = "Kralj";
    var DatumRojstva = "1989-12-02"
  }
  
  else if (stPacienta == 3) {
    var ime = "Miha";
    var priimek = "Novak";
    var datumRojstva = "2005-06-17"
  }
  
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(data) {
      var ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: {"ehrId": ehrId}
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
            $("#kreirajSporocilo").append("<p>Uspešno kreiran uporabnik <b>" + 
            ime + " " + priimek + "</b> z EhrID <b>" + ehrId + "</b>.</p>");
            $('#seznamUporabnikov').append('<option value="' + ehrId + '">' + ime + ' ' + priimek + '</option>');
            dodajMeritveVitalnihZnakov(stPacienta, ehrId);
          }
        },
        error: function(err) {
          $("#kreirajSporocilo").append("<p>Napaka '" + JSON.parse(err.responseText).userMessage + "'!</p>");
        }
      });
    }
  });
}

function dodajMeritveVitalnihZnakov(stPacienta, ehrId) {
  var merilec = "medicinska sestra Anja Sirk";
  var meritve;
  // meritve: datum, visina, teza, temperatura, sistolicni, diastolicni, kisik v krvi
  if (stPacienta == 1) {
    meritve = [
      ["2018-02-08T11:45Z", "167", "84", "36.5", "115", "73", "98.2"],
      ["2018-06-22T08:25Z", "166", "80", "38.2", "123", "78", "96.7"],
      ["2018-12-02T18:05Z", "166", "87", "38.6", "185", "123", "88.5"],
      ["2019-04-15T14:50Z", "168", "93", "36.7", "124", "77", "93.0"]
    ];
  }
  
  else if (stPacienta == 2) {
    meritve = [
      ["2016-04-29T10:25Z", "175", "53", "36.7", "112", "69", "98.1"],
      ["2017-05-05T11:00Z", "174", "58", "37.8", "108", "72", "100.0"],
      ["2018-03-22T11:30Z", "177", "61", "36.4", "115", "71", "99.2"],
      ["2019-04-07T16:40Z", "178", "59", "40.2", "124", "77", "99.2"]
    ];
  }
  
  else if (stPacienta == 3) {
    meritve = [
      ["2018-07-11T17:00Z", "155", "58", "37.2", "95", "85", "93.5"],
      ["2018-11-25T16:55Z", "156", "58", "36.6", "97", "75", "88.4"],
      ["2019-02-01T07:30Z", "156", "63", "35.6", "102", "66", "97.9"],
      ["2019-05-16T12:10Z", "158", "65", "36.4", "100", "70", "96.9"]
    ];
  }
	
	for (var i = 0; i < meritve.length; i++) {
	  var podatki = {
		  "ctx/language": "en",
		  "ctx/territory": "SI",
		  "ctx/time": meritve[i][0],
		  "vital_signs/height_length/any_event/body_height_length": meritve[i][1],
		  "vital_signs/body_weight/any_event/body_weight": meritve[i][2],
		 	"vital_signs/body_temperature/any_event/temperature|magnitude": meritve[i][3],
		  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		  "vital_signs/blood_pressure/any_event/systolic": meritve[i][4],
		  "vital_signs/blood_pressure/any_event/diastolic": meritve[i][5],
		  "vital_signs/indirect_oximetry:0/spo2|numerator": meritve[i][6]
		};
	  var parametriZahteve = {
	      ehrId: ehrId,
	      templateId: 'Vital Signs',
	      format: 'FLAT',
	      committer: merilec
	  };
	  $.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      error: function(err) {
        console.log("meritev ni bila uspešno dodana");
      }
	  });
	}
}

function preberiMeritveVitalnihZnakov(ehrId, tip, callback) {
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + tip,
     type: 'GET',
     headers: {
      "Authorization": getAuthorization()
    },
     success: function (res) {
     	if (res.length > 0) {
     	  var temperature = [];
     	  for (var i = 0; i < res.length; i++) {
     	    temperature[i] = res[res.length - 1 - i];
     	  }
     	  callback(temperature);
     	} 
     	else {
     		callback(false);
     	}
     },
     error: function() {
     	callback(false);
     }
  });
}

function vrniDatumInUro(datum) {
  return datum.slice(8, 10) + "." + datum.slice(5, 7) + "." + datum.slice(0, 4) + " " + datum.slice(11, 16);
}

function vrniDatum(datum) {
  return datum.slice(8, 10) + "." + datum.slice(5, 7) + "." + datum.slice(0, 4);
}

function zaokroziNaEnoDec(x) {
  return Math.round(x * 10) / 10;
}

function izpolniTelesnoTemperaturo(meritve) {
  $("#temperatureBody").html(""); // izprazni trenutno
  var tip, odstopanje, bolezen;
  for (var i = 0; i < meritve.length; i++) {
    var temperatura = zaokroziNaEnoDec(meritve[i]["temperature"]);
    if (temperatura >= 35.9 && temperatura <= 37.0) {
      tip = "success";
      odstopanje = "Temperatura je normalna."
      bolezen = "-";
    }
    else if (temperatura >= 37.0 && temperatura <= 38.5) {
      tip = "warning";
      odstopanje = "Temperatura je previsoka za " + String(zaokroziNaEnoDec(temperatura - 37.0)) + " °C.";
      bolezen = "Vročina (ang. febrile response).";
    }
    else if (temperatura > 38.5) {
      tip = "danger";
      odstopanje = "Temperatura je previsoka za " + String(zaokroziNaEnoDec(temperatura - 37.0)) + " °C.";
      bolezen = "Vročina (ang. febrile response).";
    }
    else if (temperatura >= 35.0) {
      tip = "warning";
      odstopanje = "Temperatura je prenizka za " + String(zaokroziNaEnoDec(35.9 - temperatura)) + " °C.";
      bolezen = "Podhladitev (ang. hypothermia).";
    }
    else {
      tip = "danger";
      odstopanje = "Temperatura je prenizka za " + String(zaokroziNaEnoDec(35.9 - temperatura)) + " °C.";
      bolezen = "Podhladitev (ang. hypothermia).";
    }
    var datum = vrniDatumInUro(meritve[i]["time"]);
    $("#temperatureBody").append('<tr class="' + tip + '"><td>' + datum + '</td><td>' +
    temperatura + ' °C</td><td>' + odstopanje + '</td><td>' + bolezen + '</td></tr>');
  }
}

function izpolniNasicenostKrviSKisikom(meritve) {
  $("#kisikBody").html(""); // izprazni trenutno
  var tip, odstopanje, bolezen;
  for (var i = 0; i < meritve.length; i++) {
    var kisik = zaokroziNaEnoDec(meritve[i]["spO2"]);
    if (kisik >= 95.0) {
      tip = "success";
      odstopanje = "Nasičenost krvi s kisikom je normalna."
      bolezen = "-";
    }
    else if (kisik >= 90.0) {
      tip = "warning";
      odstopanje = "Nasičenost krvi s kisikom bi morala biti vsaj za " + String(zaokroziNaEnoDec(95.0 - kisik)) + " % višja.";
      bolezen = "Nizka stopnja kisika v krvi (ang. hypoxemia).";
    }
    else {
      tip = "danger";
      odstopanje = "Nasičenost krvi s kisikom bi morala biti vsaj za " + String(zaokroziNaEnoDec(95.0 - kisik)) + " % višja.";
      bolezen = "Nizka stopnja kisika v krvi (ang. hypoxemia).";
    }
    var datum = vrniDatumInUro(meritve[i]["time"]);
    $("#kisikBody").append('<tr class="' + tip + '"><td>' + datum + '</td><td>' +
    kisik + ' %</td><td>' + odstopanje + '</td><td>' + bolezen + '</td></tr>');
  }
}

function izpolniBMI(meritveVisina, meritveTeza) {
  $("#bmiBody").html(""); // izprazni trenutno
  var tip, odstopanje, bolezen;
  var datumi = [];
  var vseMeritve = [];
  var bgColor = [];
  var bdColor = [];
  for (var i = 0; i < meritveVisina.length; i++) {
    var teza = meritveTeza[i]["weight"];
    var visina = meritveVisina[i]["height"] / 100;
    var bmi = zaokroziNaEnoDec(teza / (visina * visina));
    var pridobiti = zaokroziNaEnoDec((visina * visina) * 18.5 - teza);
    var izgubiti = zaokroziNaEnoDec(teza - (visina * visina) * 25.0);
    if (bmi < 16.0) {
      tip = "danger";
      odstopanje = "Pridobiti morate vsaj " + String(pridobiti) + " kg.";
      bolezen = "Zelo prenizka teža (ang. underweight).";
      bgColor.push("rgba(255, 99, 132, 0.2)");
      bdColor.push("rgb(255, 99, 132)");
    }
    else if (bmi < 18.5) {
      tip = "warning";
      odstopanje = "Pridobiti morate vsaj " + String(pridobiti) + " kg.";
      bolezen = "Prenizka teža (ang. underweight).";
      bgColor.push("rgba(255, 205, 86, 0.2)");
      bdColor.push("rgb(255, 205, 86)");
    }
    else if (bmi < 25.0) {
      tip = "success";
      odstopanje = "Teža je normalna.";
      bolezen = "-";
      bgColor.push("rgba(100, 200, 100, 0.2)");
      bdColor.push("rgb(100, 200, 100)");
    }
    else if (bmi < 30.0) {
      tip = "warning";
      odstopanje = "Izgubiti morate vsaj " + String(izgubiti) + " kg.";
      bolezen = "Previsoka teža (ang. overweight).";
      bgColor.push("rgba(255, 205, 86, 0.2)");
      bdColor.push("rgb(255, 205, 86)");
    }
    else {
      tip = "danger";
      odstopanje = "Izgubiti morate vsaj " + String(izgubiti) + " kg.";
      bolezen = "Debelost (ang. obesity).";
      bgColor.push("rgba(255, 99, 132, 0.2)");
      bdColor.push("rgb(255, 99, 132)");
    }
    var datum = vrniDatumInUro(meritveVisina[i]["time"]);
    datumi.push(vrniDatum(meritveVisina[i]["time"]));
    vseMeritve.push(bmi);
    $("#bmiBody").append('<tr class="' + tip + '"><td>' + datum + '</td><td>' + zaokroziNaEnoDec(teza) + ' kg</td><td>' +
    Math.round(meritveVisina[i]["height"]) + ' cm</td><td>' + bmi + '</td><td>' + odstopanje + '</td><td>' + bolezen + '</td></tr>');
  }
  $("#bmiChart").html('<canvas id="bmiChartCanvas" height="100"></canvas>');
  new Chart($("#bmiChartCanvas"), {
    "type": "bar",
    "data": {
      "labels": datumi,
      "datasets": [{
        "label": "BMI",
        "data": vseMeritve,
        "fill": false,
        "backgroundColor": bgColor,
        "borderColor": bdColor,
        "borderWidth": 1
      }],
    },
    "options": {
      "scales": {
        "yAxes": [{
          "ticks": {
            "min": Math.floor(Math.min(...vseMeritve) * 0.9),
            "max": Math.ceil(Math.max(...vseMeritve) * 1.1)
          }
        }]
      }
    }
  });
}

function izpolniKrvniTlak(meritve) {
  $("#krvniTlakBody").html(""); // izprazni trenutno
  var tip, odstopanje, bolezen;
  for (var i = 0; i < meritve.length; i++) {
    var sistolicni = zaokroziNaEnoDec(meritve[i]["systolic"]);
    var diastolicni = zaokroziNaEnoDec(meritve[i]["diastolic"]);
    if (sistolicni < 120 && diastolicni < 80) {
      tip = "success";
      odstopanje = "Krvni tlak je normalen."
      bolezen = "-";
    }
    else if (sistolicni < 130 && diastolicni < 80) {
      tip = "warning";
      odstopanje = "Sistolični krvni tlak bi moral biti vsaj za " + String(zaokroziNaEnoDec(sistolicni - 119.0)) + " mm[Hg] nižji.";
      bolezen = "Visok krvni tlak (ang. hypertension).";
    }
    else if (sistolicni < 120 && diastolicni >= 80) {
      tip = "danger";
      odstopanje = "Diastolični krvni tlak bi moral biti vsaj za " + String(zaokroziNaEnoDec(diastolicni - 79.0)) + " mm[Hg] nižji.";
      bolezen = "Zelo visok krvni tlak (ang. hypertension).";
    }
    else {
      tip = "danger";
      odstopanje = "Sistolični krvni tlak bi moral biti vsaj za " + String(zaokroziNaEnoDec(sistolicni - 119.0)) +
      " mm[Hg] nižji, diastolični pa vsaj za " + String(zaokroziNaEnoDec(diastolicni - 79.0)) + "  mm[Hg] nižji.";
      bolezen = "Zelo visok krvni tlak (ang. hypertension).";
    }
    var datum = vrniDatumInUro(meritve[i]["time"]);
    $("#krvniTlakBody").append('<tr class="' + tip + '"><td>' + datum + '</td><td>' + sistolicni + ' mm[Hg]</td><td>' +
    diastolicni + ' mm[Hg]</td><td>' + odstopanje + '</td><td>' + bolezen + '</td></tr>');
  }
}

function izpolniImeInPriimek(ehrId, callback) {
  $.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	   	type: 'GET',
	   	headers: {
        "Authorization": getAuthorization()
      },
	   	success: function(data) {
  			var party = data.party;
  			$("#imePacienta").html(party.firstNames + " " + party.lastNames);
  			callback(true);
  		},
  		error: function() {
  		  callback(false);
  		}
	});
}

function izpisUporabnika(ehrId) {
  izpolniImeInPriimek(ehrId, function(najdeno) {
    if (najdeno === true) {
      $("#kreirajSporocilo2").html("");
      preberiMeritveVitalnihZnakov(ehrId, "body_temperature", function(meritve) {
        izpolniTelesnoTemperaturo(meritve);
      });
      preberiMeritveVitalnihZnakov(ehrId, "blood_pressure", function(meritve) {
        izpolniKrvniTlak(meritve);
      });
      preberiMeritveVitalnihZnakov(ehrId, "height", function(meritveVisina) {
        preberiMeritveVitalnihZnakov(ehrId, "weight", function(meritveTeza) {
          izpolniBMI(meritveVisina, meritveTeza);
        });
      });
      preberiMeritveVitalnihZnakov(ehrId, "spO2", function(meritve) {
        izpolniNasicenostKrviSKisikom(meritve);
      });
      $('#izpisUporabnika').css('display', 'block');
    }
    else {
      $("#kreirajSporocilo2").html("Uporabnika ni bilo mogoče najti!");
      $('#izpisUporabnika').css('display', 'none');
    }
  });
}

function izpisBolezni(bolezen) {
  var duckUrl = "https://api.duckduckgo.com/?q=" + bolezen + "&format=json";
  $.ajax({
    url: duckUrl,
    dataType: "jsonp",
    success: function(data) {
      var naslov = data.Heading;
      var info = data.Abstract;
      if (info == "") {
        $("#izpisBolezni").html("Bolezni ni bilo mogoče najti.");
      }
      else {
        $("#izpisBolezni").html("<h4>" + ((naslov == "") ? bolezen : naslov) + "</h4><p>" + info + "</p>");
      }
    },
    error: function() {
      $("#izpisBolezni").html("Prišlo je do problema pri poizvedbi.");
    }
  });
}

$(document).ready(function() {
  
  $("#seznamUporabnikov").change(function() {
    var ehrId = $("#seznamUporabnikov").val();
    izpisUporabnika(ehrId);
  });
  
  $("#gumbEhrId").click(function() {
    var ehrId = $("#vnesiEhrId").val();
    izpisUporabnika(ehrId);
  });
  
  $("#seznamBolezni").change(function() {
    var bolezen = $("#seznamBolezni").val();
    izpisBolezni(bolezen);
  });
  
  $("#gumbBolezenIsci").click(function() {
    var bolezen = $("#vnesiBolezen").val();
    izpisBolezni(bolezen);
  });
  
});
